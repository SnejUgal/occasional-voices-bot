use dotenv::dotenv;
use serde::Deserialize;
use std::env::{self, VarError};
use tbot::{
    types::inline_query::{
        self,
        result::{self, voice::Fresh},
    },
    Bot,
};
use tokio::fs;

#[derive(Debug, Deserialize)]
struct Voice {
    title: String,
    url: String,
}

#[derive(Debug, Deserialize)]
struct Configuration {
    voices: Vec<Voice>,
}

#[tokio::main]
async fn main() {
    let _ = dotenv();
    let configuration = read_configuration().await;

    let mut bot = Bot::from_env("BOT_TOKEN").stateful_event_loop(configuration);

    bot.inline(|context, configuration| async move {
        let ids: Vec<_> = (0..configuration.voices.len())
            .map(|x| x.to_string())
            .collect();

        let results: Vec<_> = configuration
            .voices
            .iter()
            .enumerate()
            .map(|(nth, voice)| {
                inline_query::Result::new(
                    &ids[nth],
                    result::Voice::fresh(&voice.title, Fresh::new(&voice.url)),
                )
            })
            .collect();

        context.answer(&results).call().await.unwrap();
    });

    match (extract_webhook_url(), extract_webhook_port()) {
        (Some(url), Some(port)) => bot.webhook(&url, port).http().start().await.unwrap(),
        (None, None) => bot.polling().start().await.unwrap(),
        (Some(_), None) => panic!("WEBHOOK_URL set without WEBHOOK_PORT"),
        (None, Some(_)) => panic!("WEBHOOK_PORT set without WEBHOOK_URL"),
    };
}

async fn read_configuration() -> Configuration {
    let file_contents = fs::read("./occasional-voices-bot.toml")
        .await
        .expect("failed to read ./occasional-voices-bot.toml");

    toml::de::from_slice(&file_contents).expect("failed to parse the configuration file")
}

fn extract_webhook_url() -> Option<String> {
    match env::var("WEBHOOK_URL") {
        Ok(url) => Some(url),
        Err(VarError::NotPresent) => None,
        Err(VarError::NotUnicode(_)) => panic!("WEBHOOK_URL contains invalid UTF-8"),
    }
}

fn extract_webhook_port() -> Option<u16> {
    match env::var("WEBHOOK_PORT") {
        Ok(port) => Some(port.parse().expect("failed to parse WEBHOOK_PORT")),
        Err(VarError::NotPresent) => None,
        Err(VarError::NotUnicode(_)) => panic!("WEBHOOK_PORT contains invalid UTF-8"),
    }
}
